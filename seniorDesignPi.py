import requests
import sys
import re
import io
import RPi.GPIO as GPIO
import threading
import random
import serial
import string
import getpass
import time

GPIO.setmode(GPIO.BOARD)
GPIO.setup(15,GPIO.OUT)
GPIO.setup(16,GPIO.OUT)
GPIO.setup(18,GPIO.OUT)
GPIO.setup(22,GPIO.OUT)
GPIO.setup(24,GPIO.OUT)
GPIO.setup(26,GPIO.OUT)

URL_LOGIN = 'https://trine-smart-power.herokuapp.com/login/'
URL_ROOT = 'https://trine-smart-power.herokuapp.com'
URL_NEW_CHARGE = 'https://trine-smart-power.herokuapp.com/battery_charges/new'
URL_POST_CHARGE = 'https://trine-smart-power.herokuapp.com/battery_charges'
URL_USER = 'https://trine-smart-power.herokuapp.com/profile'

needsToReadCharge = True
mostRecentCharge = 0
batteryRead = 1000

serialBus = serial.Serial("/dev/ttyAMA0",9600)
serialBus.open()
sio = io.TextIOWrapper(io.BufferedRWPair(serialBus,serialBus,1))

print '------------------------------------'
print '            Start Program           '
print '------------------------------------'

EMAIL = raw_input('Email: ')
PASSWORD = getpass.getpass()

def calculateCharge(voltage):
	temp = (-22.689*(voltage**3))+(819.3*(voltage**2))-(9777.3*voltage)+38596
	temp = int(temp)
	if(temp > 100):
		temp = 100
	if(temp < 0):
		temp = 0
	return temp

def readBatteryCharge():
	global needsToReadCharge
	threading.Timer(15.0,readBatteryCharge).start()

	print('go')
	needsToReadCharge = True
		
def main():

	######################################################
	#		  SETUP / LOGIN
	######################################################

	global needsToReadCharge
	global mostRecentCharge

	session = requests.session(config={'verbose': sys.stderr})
	r = session.get(URL_ROOT)

	matchme = 'meta name="csrf-token" content="(.*)" /'
	csrf = re.search(matchme,str(r.text))

	login_data = {
		'utf8': True,
		'authenticity_token': csrf.group(1),
		'session[email]': EMAIL,
		'session[password]': PASSWORD,
		'commit': 'Log in'
	}

	r = session.post(URL_LOGIN, data=login_data)
	
	readBatteryCharge()

	######################################################
	#		BEGIN MAIN LOOP
	######################################################
	
	batteryRead = 1000
	while(1):
		if(needsToReadCharge):
			r = session.get(URL_NEW_CHARGE)
			csrf = re.search(matchme,str(r.text))
			#GPIO.output(22,GPIO.HIGH)
			#line = sio.readline()
			#line = 800
			#GPIO.output(22,GPIO.LOW)
			batteryRead = batteryRead-10
			print batteryRead
			if(batteryRead < 900):
				batteryRead = 1000
			line = batteryRead
			temp = (int(line))*12.81/1024
			print temp
			chargeNumber = calculateCharge(temp)
			mostRecentCharge = chargeNumber
			print chargeNumber
			post_data ={
				'utf8': True,
				'authenticity_token': csrf.group(1),
				'battery_charge[charge]': chargeNumber,
				'commit': 'Create a Load'
			}
			r = session.post(URL_POST_CHARGE, data=post_data)
			needsToReadCharge = False

		r = session.get(URL_USER)

		loadStateModel1 = 'Load 1: (.*) </b>'
		loadStateModel2 = 'Load 2: (.*) </b>'
		loadStateModel3 = 'Load 3: (.*) </b>'
		loadStateModel4 = 'Load 4: (.*) </b>'
		load1 = re.search(loadStateModel1,str(r.text))
		load2 = re.search(loadStateModel2,str(r.text))
		load3 = re.search(loadStateModel3,str(r.text))
		load4 = re.search(loadStateModel4,str(r.text))

		if(load1.group(1) == 'On'):
			GPIO.output(15,GPIO.HIGH)
		else:
			GPIO.output(15,GPIO.LOW)

		if(load2.group(1) == 'On'):
			GPIO.output(16,GPIO.HIGH)
		else:
			GPIO.output(16,GPIO.LOW)
		
		if(load3.group(1) == 'On'):
			GPIO.output(18,GPIO.HIGH)
		else:
			GPIO.output(18,GPIO.LOW)
		if(load4.group(1) == 'On' and mostRecentCharge > 20):
			GPIO.output(24,GPIO.LOW)#Redundancy to
			GPIO.output(26,GPIO.LOW)#ensure safety
			GPIO.output(24,GPIO.HIGH)
		else:
			GPIO.output(24,GPIO.LOW)#Redundancy to
			GPIO.output(26,GPIO.LOW)#ensure safety
			GPIO.output(26,GPIO.HIGH)
		
		time.sleep(3)

main()